#ifndef _VERS_ID_H
#define _VERS_ID_H

#if !defined(VERSION) || !defined(D2X_MAJOR) || !defined(D2X_MINOR) || !defined(D2X_MICRO)
#error At least one version define not found
#endif

#define D2X_NAME		"D2X-XL "

#define VERSION_TYPE		"Full Version"
#define DESCENT_VERSION D2X_NAME VERSION
#define D2X_IVER			(D2X_MAJOR * 100000 + D2X_MINOR * 1000 + D2X_MICRO)

#endif //_VERS_ID_H
