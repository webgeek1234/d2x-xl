/*
THE COMPUTER CODE CONTAINED HEREIN IS THE SOLE PROPERTY OF PARALLAX
SOFTWARE CORPORATION ("PARALLAX").  PARALLAX, IN DISTRIBUTING THE CODE TO
END-USERS, AND SUBJECT TO ALL OF THE TERMS AND CONDITIONS HEREIN, GRANTS A
ROYALTY-FREE, PERPETUAL LICENSE TO SUCH END-USERS FOR USE BY SUCH END-USERS
IN USING, DISPLAYING,  AND CREATING DERIVATIVE WORKS THEREOF, SO LONG AS
SUCH USE, DISPLAY OR CREATION IS FOR NON-COMMERCIAL, ROYALTY OR REVENUE
FREE PURPOSES.  IN NO EVENT SHALL THE END-USER USE THE COMPUTER CODE
CONTAINED HEREIN FOR REVENUE-BEARING PURPOSES.  THE END-USER UNDERSTANDS
AND AGREES TO THE TERMS HEREIN AND ACCEPTS THE SAME BY USE OF THIS FILE.
COPYRIGHT 1993-1999 PARALLAX SOFTWARE CORPORATION.  ALL RIGHTS RESERVED.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#if defined(__unix__) || defined(__macosx__)
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#endif

#ifdef __macosx__
#	include "SDL/SDL_main.h"
#	include "SDL/SDL_keyboard.h"
#	include "FolderDetector.h"
#else
#	include "SDL_main.h"
#	include "SDL_keyboard.h"
#endif
#include "descent.h"
#include "u_mem.h"
#include "strutil.h"
#include "key.h"
#include "timer.h"
#include "error.h"
#include "segpoint.h"
#include "screens.h"
#include "texmap.h"
#include "texmerge.h"
#include "menu.h"
#include "iff.h"
#include "pcx.h"
#include "args.h"
#include "hogfile.h"
#include "ogl_defs.h"
#include "ogl_lib.h"
#include "ogl_shader.h"
#include "sdlgl.h"
#include "text.h"
#include "newdemo.h"
#include "objrender.h"
#include "renderthreads.h"
#include "network.h"
#include "gamefont.h"
#include "kconfig.h"
#include "mouse.h"
#include "joy.h"
#include "desc_id.h"
#include "joydefs.h"
#include "gamepal.h"
#include "movie.h"
#include "compbit.h"
#include "playerprofile.h"
#include "tracker.h"
#include "rendermine.h"
#include "sphere.h"
#include "endlevel.h"
#include "interp.h"
#include "autodl.h"
#include "hiresmodels.h"
#include "soundthreads.h"
#include "songs.h"
#include "cvar.h"
#include "lightcluster.h"

// Launcher parsing code based off the code in fs2_open: scp
enum
{
	// DO NOT CHANGE ANYTHING ABOUT THESE FIRST TWO OR WILL MESS UP THE LAUNCHER
	EASY_DEFAULT  =  1 << 1,
	EASY_ALL_ON   =  1 << 2,

	EASY_MEM_ON   =  1 << 3,
	EASY_MEM_OFF  =  1 << 4,

	// Add new flags here

	// Combos
	EASY_MEM_ALL_ON  = EASY_ALL_ON  | EASY_MEM_ON,
	EASY_DEFAULT_MEM = EASY_DEFAULT | EASY_MEM_OFF
};

#define BUILD_CAP_OPENAL	(1<<0)
#define BUILD_CAP_NO_D3D	(1<<1)
#define BUILD_CAP_NEW_SND	(1<<2)

typedef struct
{
	// DO NOT CHANGE THE SIZE OF THIS STRING!
	char name[32];

} EasyFlag;

EasyFlag easy_flags[] =
{
	{ "Custom" },
	{ "Default (All features off)" },
	{ "All features on" },
	{ "High memory usage features on" },
	{ "High memory usage features off" }
};

// DO NOT CHANGE **ANYTHING** ABOUT THIS STRUCTURE AND ITS CONTENT
typedef struct
{
	char  name[20];		// The actual flag
	char  desc[40];		// The text that will appear in the launcher (unless its blank, other name is shown)
	bool  d2x_only;		// true if this is a d2x only feature
	int   on_flags;		// Easy flag which will turn this feature on
	int   off_flags;	// Easy flag which will turn this feature off
	char  type[16];		// Launcher uses this to put flags under different headings
	char  web_url[256];	// Link to documentation of feature (please use wiki or somewhere constant)

} Flag;

// Please group them by type, ie graphics, gameplay etc, maximum 20 different types
Flag exe_params[] =
{
	{ "-FSAA",		"Enable fullscreen antialiasing",		true,	EASY_ALL_ON,		EASY_DEFAULT_MEM,	"Graphics",		"", },
	{ "-hires_textures",	"Use high resolution textures",			true,	EASY_MEM_ALL_ON,	EASY_DEFAULT_MEM,	"Graphics",		"", },
	{ "-hires_models",	"Use high poly models",				true,	EASY_MEM_ALL_ON,	EASY_DEFAULT_MEM,	"Graphics",		"", },
#ifdef OCULUS_RIFT
	{ "-oculus_rift",  	"Enable VR for the oculus rift",		true,	0,			EASY_DEFAULT,		"Graphics",		"", },
#endif

	{ "-compress_data",	"Compress computed lightmaps and meshes",	true,	0,			EASY_DEFAULT_MEM,	"Game Speed",		"", },

	{ "-autodemos",		"Automatically play stuff when idling",		true,	0,			EASY_DEFAULT,		"Gameplay",		"", },
	{ "-pured2",		"Act like the original Descent II",		true,	0,			EASY_DEFAULT,		"Gameplay",		"", },
	{ "-secretsave",	"Allow saving in secret levels",		true,	EASY_DEFAULT,		0,			"Gameplay",		"", },

	{ "-nomusic",		"Disable music",				false,	0,			EASY_DEFAULT,		"Audio",		"", },
	{ "-noredbook",		"Disable redbook (cd) audio",			false,	0,			EASY_DEFAULT,		"Audio",		"", },
	{ "-nosound",		"Disable all sound",				false,	0,			EASY_DEFAULT,		"Audio",		"", },
	{ "-sdl_mixer",		"Enable audio playback via sdl_mixer",		true,	EASY_DEFAULT,		0,			"Audio",		"", },

	{ "-norankings",	"Disable multiplayer ranking system",		true,	0,			EASY_DEFAULT,		"Multiplayer",		"", },

	{ "-disable_robots",	"Disable robot AI",				true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-disable_powerups",	"Remove all powerups from the game",		true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-nograbmouse",	"Don't lock mouse to window when ingame",	true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-macdata",		"Data files are in mac format",			true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-nomovies",		"Disable intro and endlevel movies",		true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-noscreens",		"Disable briefing screens",			true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-revert_demos",	"Revert demos to the D2 legacy format",		true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-standalone",	"Don't load any copyrighted data",      	true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-windowed",		"Disable fullscreen rendering",			true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-debug",		"Output lots of debugging information",		true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
	{ "-verbose",		"Output some debugging information",		true,	0,			EASY_DEFAULT,		"Troubleshoot",		"", },
};

// ----------------------------------------------------------------------------

void EvalAutoNetGameArgs (void)
{
	int32_t	t, bHaveIp = 0;
	char	*p;
#if 0
	static const char *pszTypes [] = {"anarchy", "coop", "ctf", "ctf+", "hoard", "entropy", NULL};
	static const char	*pszConnect [] = {"ipx", "udp", "", "multicast", NULL};
#endif
memset (&gameData.multiplayer.autoNG, 0, sizeof (gameData.multiplayer.autoNG));
if ((t = FindArg ("-ng_player")) && (p = appConfig [t+1])) {
	strncpy (gameData.multiplayer.autoNG.szPlayer, appConfig [t+1], 8);
	gameData.multiplayer.autoNG.szPlayer [8] = '\0';
	}
if ((t = FindArg ("-ng_mission")) && (p = appConfig [t+1])) { // was -ng_file
	strncpy (gameData.multiplayer.autoNG.szFile, appConfig [t+1], FILENAME_LEN - 1);
	gameData.multiplayer.autoNG.szFile [FILENAME_LEN - 1] = '\0';
	}
#if 0
if ((t = FindArg ("-ng_mission")) && (p = appConfig [t+1])) {
	strncpy (gameData.multiplayer.autoNG.szMission, appConfig [t+1], 12);
	gameData.multiplayer.autoNG.szMission [12] = '\0';
	}
#endif
if ((t = FindArg ("-ng_level")))
	gameData.multiplayer.autoNG.nLevel = NumArg (t, 1);
else
	gameData.multiplayer.autoNG.nLevel = 1;
if ((t = FindArg ("-ng_server")) && (p = appConfig [t+1]))
	bHaveIp = stoip (appConfig [t+1], gameData.multiplayer.autoNG.ipAddr, &gameData.multiplayer.autoNG.nPort);

if ((t = FindArg ("-ng_team")))
	gameData.multiplayer.autoNG.bTeam = NumArg (t, 1);

#if 0 // game host parameters
if ((t = FindArg ("-ng_name")) && (p = appConfig [t+1])) {
	strncpy (gameData.multiplayer.autoNG.szName, appConfig [t+1], 80);
	gameData.multiplayer.autoNG.szName [80] = '\0';
	}
if ((t = FindArg ("-ng_host")))
	gameData.multiplayer.autoNG.bHost = !strcmp (p, "host");
if ((t = FindArg ("-ng_type")) && (p = appConfig [t+1])) {
	strlwr (p);
	for (t = 0; pszTypes [t]; t++)
		if (!strcmp (p, pszTypes [t])) {
			gameData.multiplayer.autoNG.uType = t;
			break;
			}
	}
if ((t = FindArg ("-ng_protocol")) && (p = appConfig [t+1])) {
	strlwr (p);
	for (t = 0; pszTypes [t]; t++)
		if (*pszConnect [t] && !strcmp (p, pszConnect [t])) {
			gameData.multiplayer.autoNG.uConnect = t;
			break;
			}
	}
else
#endif
	gameData.multiplayer.autoNG.uConnect = 1;

if (gameData.multiplayer.autoNG.bHost)
	gameData.multiplayer.autoNG.bValid =
		*gameData.multiplayer.autoNG.szPlayer &&
		*gameData.multiplayer.autoNG.szName &&
		*gameData.multiplayer.autoNG.szFile &&
		*gameData.multiplayer.autoNG.szMission;
else
	gameData.multiplayer.autoNG.bValid =
		//*gameData.multiplayer.autoNG.szPlayer &&
		//*gameData.multiplayer.autoNG.szFile &&
		//gameData.multiplayer.autoNG.nLevel &&
		bHaveIp;

if (gameData.multiplayer.autoNG.bValid)
	gameOptions [0].movies.nLevel = 0;
}

// ----------------------------------------------------------------------------

void EvalMultiplayerArgs (void)
{
if (FindArg ("-norankings"))
	gameOptions [0].multi.bNoRankings = 1;
EvalAutoNetGameArgs ();
}

// ----------------------------------------------------------------------------

void EvalMovieArgs (void)
{
	int32_t	t;

if ((t = FindArg ("-nomovies")))
	gameOptions [0].movies.nLevel = 2 - NumArg (t, 1);
if ((t = FindArg ("-movies")))
	gameOptions [0].movies.nLevel = NumArg (t, 2);
if (gameOptions [0].movies.nLevel < 0)
	gameOptions [0].movies.nLevel = 0;
else if (gameOptions [0].movies.nLevel > 2)
	gameOptions [0].movies.nLevel = 2;
if (FindArg ("-subtitles"))
	gameOptions [0].movies.bSubTitles = NumArg (t, 1);
if ((t = FindArg ("-movie_quality")))
	gameOptions [0].movies.nQuality = NumArg (t, 0);
if (gameData.multiplayer.autoNG.bValid > 0)
	gameOptions [0].movies.nLevel = 0;
}

// ----------------------------------------------------------------------------

void EvalSoundArgs (void)
{
	int32_t	t;

#if USE_SDL_MIXER
#	ifdef __macosx__
void * volatile function_p = (void *)&(Mix_OpenAudio);
if (function_p == NULL) {

	// the SDL_mixer framework is not present,
	// so regardless of what conf.h or d2x.ini says,
	// we don't want to use SDL_mixer

	gameOptions [0].sound.bUseSDLMixer = 0;
	}
else
#	endif //__macosx__
if ((t = FindArg ("-sdl_mixer")))
	gameOptions [0].sound.bUseSDLMixer = NumArg (t, 1);
#endif //USE_SDL_MIXER
if ((t = FindArg ("-midifix")))
	gameStates.sound.bMidiFix = NumArg (t, 1);
if ((t = FindArg ("-dynamic_sound")))
	gameStates.sound.bDynamic = NumArg (t, 1);
else
	gameStates.sound.bDynamic = 1;
if ((t = FindArg ("-noredbook")))
	gameOptions [0].sound.bUseRedbook = 0;
#if USE_SDL_MIXER
if (gameOptions [0].sound.bUseSDLMixer) {
	if ((t = FindArg ("-hires_sound")))
		gameOptions [0].sound.bHires [0] =
		gameOptions [0].sound.bHires [1] = NumArg (t, 2);
	}
#endif
}

// ----------------------------------------------------------------------------

void EvalMusicArgs (void)
{
	int32_t	t;
	char	*p;

if ((t = FindArg ("-nomusic")))
	gameStates.sound.audio.bNoMusic = NumArg (t, 0) == 0;
if ((t = FindArg ("-playlist")) && (p = appConfig [t+1]))
	songManager.LoadUserPlaylist (p);
if ((t = FindArg ("-introsong")) && (p = appConfig [t+1]))
	strncpy (songManager.IntroSong (), p, FILENAME_LEN);
if ((t = FindArg ("-briefingsong")) && (p = appConfig [t+1]))
	strncpy (songManager.BriefingSong (), p, FILENAME_LEN);
if ((t = FindArg ("-creditssong")) && (p = appConfig [t+1]))
	strncpy (songManager.CreditsSong (), p, FILENAME_LEN);
if ((t = FindArg ("-menusong")) && (p = appConfig [t+1]))
	strncpy (songManager.MenuSong (), p, FILENAME_LEN);
}

// ----------------------------------------------------------------------------

void EvalMenuArgs (void)
{
	int32_t	t;

if (!gameStates.app.bNostalgia) {
	if ((t = FindArg ("-menustyle")))
		gameOptions [0].menus.nStyle = NumArg (t, 1);
	else
		gameOptions [0].menus.nStyle = 1;
	}
if ((t = FindArg ("-fademenus")))
	gameOptions [0].menus.nFade = NumArg (t, 0);
if ((t = FindArg ("-altbg_alpha")) && *appConfig [t+1]) {
	gameOptions [0].menus.altBg.alpha = atof (appConfig [t+1]);
	if (gameOptions [0].menus.altBg.alpha < 0)
		gameOptions [0].menus.altBg.alpha = -1.0;
	else if ((gameOptions [0].menus.altBg.alpha == 0) || (gameOptions [0].menus.altBg.alpha > 1.0))
		gameOptions [0].menus.altBg.alpha = 1.0;
	}
if ((t = FindArg ("-altbg_brightness")) && *appConfig [t+1]) {
	gameOptions [0].menus.altBg.brightness = atof (appConfig [t+1]);
	if ((gameOptions [0].menus.altBg.brightness <= 0) || (gameOptions [0].menus.altBg.brightness > 1.0))
		gameOptions [0].menus.altBg.brightness = 1.0;
	}
if ((t = FindArg ("-altbg_grayscale")))
	gameOptions [0].menus.altBg.grayscale = NumArg (t, 1);
if ((t = FindArg ("-altbg_cartoonize")))
	gameOptions [0].menus.altBg.bCartoonize = NumArg (t, 1);
if ((t = FindArg ("-altbg_name")) && *appConfig [t+1])
	strncpy (gameOptions [0].menus.altBg.szName [0], appConfig [t+1], sizeof (gameOptions [0].menus.altBg.szName [0]));
if ((t = FindArg ("-use_swapfile")))
	gameStates.app.bUseSwapFile = NumArg (t, 1);
}

// ----------------------------------------------------------------------------

void EvalGameplayArgs (void)
{
	int32_t	t;

if ((t = FindArg ("-briefings")))
	gameOpts->gameplay.bSkipBriefingScreens = !NumArg (t, 1);
if ((t = FindArg ("-noscreens")))
	gameOpts->gameplay.bSkipBriefingScreens = NumArg (t, 1);
if ((t = FindArg ("-secretsave")))
	gameOptions [0].gameplay.bSecretSave = NumArg (t, 1);
if ((t = FindArg ("-disable_robots")) && NumArg (t, 1))
	gameStates.app.bGameSuspended |= SUSP_ROBOTS;
if ((t = FindArg ("-disable_powerups")) && NumArg (t, 1))
	gameStates.app.bGameSuspended |= SUSP_POWERUPS;
}

// ----------------------------------------------------------------------------

void EvalInputArgs (void)
{
	int32_t	t;

if ((t = FindArg ("-nograbmouse")))
	gameStates.input.bGrabMouse = NumArg (t, 0);
}

// ----------------------------------------------------------------------------

void EvalOglArgs (void)
{
	int32_t	t;

#if DBG
if ((t = FindArg ("-gl_alttexmerge")))
	gameOpts->ogl.bGlTexMerge = NumArg (t, 1);
#endif
#if 0
// lowmem only really makes senes together with limiting texture preloading
if ((t = FindArg ("-lowmem")))
	ogl.m_states.bLowMemory = NumArg (t, 1);
// this parameter can lead to the game briefly pausing everytime a new weapon, robot etc. becomes visible
// that is very annoying and not immediately understandable to players
if ((t = FindArg ("-preload_textures")))
	ogl.m_states.nPreloadTextures = NumArg (t, 6);
else
#endif
	ogl.m_states.nPreloadTextures = 6;
if ((t = FindArg ("-FSAA")))
	ogl.m_states.bFSAA = NumArg (t, 1);
if ((t = FindArg ("-quad_buffering")))
	ogl.m_features.bQuadBuffers = NumArg (t, 1);
}

// ----------------------------------------------------------------------------

void EvalDemoArgs (void)
{
	int32_t	t;

if ((t = FindArg ("-revert_demos")))
	gameOpts->demo.bRevertFormat = NumArg (t, 1);
if ((t = FindArg ("-auto_demos")))
	gameStates.app.bAutoDemos = NumArg (t, 1);
}

// ----------------------------------------------------------------------------

void EvalRenderArgs (void)
{
	int32_t	t;

if ((t = FindArg ("-hires_textures")))
	gameOptions [0].render.textures.bUseHires [0] =
	gameOptions [0].render.textures.bUseHires [1] = NumArg (t, 1);
if ((t = FindArg ("-hires_models")))
	gameOptions [0].render.bHiresModels [0] =
	gameOptions [0].render.bHiresModels [1] = NumArg (t, 1);
if ((t = FindArg ("-model_quality")) && *appConfig [t+1])
	gameStates.render.nModelQuality = NumArg (t, 3);
#if 0
if ((t = FindArg ("-gl_texcompress")))
	ogl.m_features.bTextureCompression.Apply (NumArg (t, 1));
#endif
if ((t = FindArg ("-configure_light_components")))
	gameOptions [0].render.color.bConfigurable = NumArg (t, 1);
gameOptions [0].render.bUseShaders = 1;
gameStates.app.bReadOnly = 0;
gameStates.app.bCacheTextures = 1;
gameStates.app.bCacheModelData = 1;
gameStates.app.bCacheMeshes = 1;
gameStates.app.bCacheLightmaps = 1;
gameStates.app.bCacheLights = 1;
#if 1 //DBG
if ((t = FindArg ("-readonly"))) {
	gameStates.app.bReadOnly = 1;
	gameStates.app.bCacheTextures = 0;
	gameStates.app.bCacheModelData = 0;
	gameStates.app.bCacheMeshes = 0;
	gameStates.app.bCacheLightmaps = 0;
	gameStates.app.bCacheLights = 0;
	}
#else
	gameStates.app.bCacheTextures = NumArg (t, 1);
if ((t = FindArg ("-cache_textures")))
	gameStates.app.bCacheTextures = NumArg (t, 1);
if ((t = FindArg ("-cache_models")))
	gameStates.app.bCacheModelData = NumArg (t, 1);
if ((t = FindArg ("-cache_meshes")))
	gameStates.app.bCacheMeshes = NumArg (t, 1);
if ((t = FindArg ("-cache_lightmaps")))
	gameStates.app.bCacheLightmaps = NumArg (t, 1);
if ((t = FindArg ("-cache_lights")))
	gameStates.app.bCacheLights = NumArg (t, 1);
#endif
if ((t = FindArg ("-use_shaders")))
	gameOptions [0].render.bUseShaders = NumArg (t, 1);
if ((t = FindArg ("-enable_freecam")))
	gameStates.render.bEnableFreeCam = NumArg (t, 1);
if ((t = FindArg ("-oculus_rift")))
	gameOptions [0].render.bUseRift = NumArg (t, 1);
}

// ----------------------------------------------------------------------------

void EvalShipArgs (void)
{
	const char*	szShipArgs [] = {"-medium_ship", "-light_ship", "-heavy_ship"};
	int32_t	t;
	char	*p;

for (int32_t i = 0; i < MAX_SHIP_TYPES; i++) {
	if ((t = FindArg (szShipArgs [i])) && (p = appConfig [t+1]) && *p) {
		strncpy (gameData.modelData.szShipModels [i], appConfig [t+1], FILENAME_LEN);
		strlwr (gameData.modelData.szShipModels [i]);
		replacementModels [i].pszHires = gameData.modelData.szShipModels [i];
		}
	}
}

// ----------------------------------------------------------------------------

void EvalAppArgs (void)
{
	int32_t	t;

#if 0
if ((t = FindArg ("-gpgpu_lights")))
	ogl.m_states.bVertexLighting = NumArg (t, 1);
#endif
#ifdef __unix__
if ((t = FindArg ("-linux_msgbox")))
	gameStates.app.bLinuxMsgBox = NumArg (t, 1);
#endif
if ((t = FindArg ("-show_version_info")))
	gameStates.app.bShowVersionInfo = NumArg (t, 1);
if ((t = FindArg ("-check_setup")))
	gameStates.app.bCheckAndFixSetup = NumArg (t, 1);
#if 0
if ((t = FindArg ("-expertmode")))
	gameOpts->app.bExpertMode = NumArg (t, 1);
#endif
if ((t = FindArg ("-pured2")))
	SetNostalgia (3);
else if ((t = FindArg ("-nostalgia")))
	SetNostalgia (appConfig [t+1] ? NumArg (t, 0) : 1);
else
	SetNostalgia (0);

if (!gameStates.app.bNostalgia && (t = FindArg ("-standalone")))
	gameStates.app.bStandalone = NumArg (t, 1);
else
	gameStates.app.bStandalone = 0;

#ifdef _OPENMP //MULTI_THREADED
if ((t = FindArg ("-multithreaded"))) {
	gameStates.app.nThreads = NumArg (t, 1);
	gameStates.app.bMultiThreaded = (gameStates.app.nThreads > 0);
	if (gameStates.app.nThreads == 0)
		gameStates.app.nThreads = 1;
	else if (gameStates.app.nThreads == 1)
		gameStates.app.nThreads = MAX_THREADS;
	else if (gameStates.app.nThreads > MAX_THREADS)
		gameStates.app.nThreads = MAX_THREADS;
	}
else
#endif
	{
	gameStates.app.nThreads = 1;
	gameStates.app.bMultiThreaded = 0;
	}
if ((t = FindArg ("-tracelevel")))
	gameStates.app.nTraceLevel = NumArg (t, 0);
else
	gameStates.app.nTraceLevel = 0;
if ((t = FindArg ("-nosound")))
	gameStates.app.bUseSound = (NumArg (t, 1) == 0);
if ((t = FindArg ("-progress_bars")))
	gameStates.app.bProgressBars = NumArg (t, 1);
if ((t = FindArg ("-altLanguage")))
	gameStates.app.bEnglish = (NumArg (t, 1) == 0);

if ((t = FindArg ("-auto_hogfile"))) {
	strcpy (szAutoHogFile, "missions/");
	strcat (szAutoHogFile, appConfig [t+1]);
	if (*szAutoHogFile && !strchr (szAutoHogFile, '.'))
		strcat (szAutoHogFile, ".hog");
	}
if ((t = FindArg ("-auto_mission"))) {
	char		c = *appConfig [++t];
	int32_t	bDelim = ((c == '\'') || (c == '"'));

	strcpy (szAutoMission, &appConfig [t][bDelim]);
	if (bDelim)
		szAutoMission [strlen (szAutoMission) - 1] = '\0';
	if (*szAutoMission && !strchr (szAutoMission, '.'))
		strcat (szAutoMission, ".rl2");
	}
if (FindArg ("-debug"))
	CCvar::Register (const_cast<char*>("con_threshold"), 2.0);
else if (FindArg ("-verbose"))
	CCvar::Register (const_cast<char*>("con_threshold"), 1.0);
else
	CCvar::Register (const_cast<char*>("con_threshold"), -1.0);
if ((t = FindArg ("-autodemo"))) {
	gameData.demoData.bAuto = 1;
	strncpy (gameData.demoData.fnAuto, *appConfig [t+1] ? appConfig [t+1] : "descent.dem", sizeof (gameData.demoData.fnAuto));
	}
else
	gameData.demoData.bAuto = 0;
gameStates.app.bMacData = FindArg ("-macdata");
if ((t = FindArg ("-compress_data"))) {
	gameStates.app.bCompressData = (NumArg (t, 1) == 1);
	PrintLog (0, "gameStates.app.bCompressData = 1\n");
	}
else
	gameStates.app.bCompressData = 1;
if (gameStates.app.bNostalgia)
	gameData.segData.nMaxSegments = MAX_SEGMENTS_D2;
}

// ----------------------------------------------------------------------------

bool EvalArgs (void)
{
    // DO THIS FIRST to avoid unrecognized flag warnings when just getting flag file
	if ( FindArg ("-get_flags") ) {
		FILE *fp = fopen("flags.lch","w");

		if (fp == NULL) {
			printf("Error creating flag list for launcher");
			return false;
		}

		int easy_flag_size	= sizeof(EasyFlag);
		int flag_size		= sizeof(Flag);

		int num_easy_flags	= sizeof(easy_flags) / easy_flag_size;
		int num_flags		= sizeof(exe_params) / flag_size;

		// Launcher will check its using structures of the same size
		fwrite(&easy_flag_size, sizeof(int), 1, fp);
		fwrite(&flag_size, sizeof(int), 1, fp);

		fwrite(&num_easy_flags, sizeof(int), 1, fp);
		fwrite(&easy_flags, sizeof(easy_flags), 1, fp);

		fwrite(&num_flags, sizeof(int), 1, fp);
		fwrite(&exe_params, sizeof(exe_params), 1, fp);

		{
			// cheap and bastardly cap check for builds
			// (needs to be compatible with older Launchers, which means having
			//  this implies an OpenAL build for old Launchers)
			uint8_t build_caps = 0;

			/* portej05 defined this always */
			build_caps |= BUILD_CAP_OPENAL;
			build_caps |= BUILD_CAP_NO_D3D;
			build_caps |= BUILD_CAP_NEW_SND;


			fwrite(&build_caps, 1, 1, fp);
		}

		fflush(fp);
		fclose(fp);

		return false;
	}

EvalAppArgs ();
EvalGameplayArgs ();
EvalInputArgs ();
EvalMultiplayerArgs ();
EvalMenuArgs ();
EvalMovieArgs ();
EvalOglArgs ();
EvalRenderArgs ();
EvalSoundArgs ();
EvalMusicArgs ();
EvalDemoArgs ();
EvalShipArgs ();

return true;
}

// ----------------------------------------------------------------------------
//eof
