%define input_path  /usr/share/descent2
%define output_path /usr/share/descent2
%define addon_url http://descent2.de/files

Name:           descent-data
Version:        1.0
Release:        2%{dist}
Summary:        Descent data
# The initial code released by Parallex had a non-commercial restriction
License:        Proprietary
URL:            ???
Source:         CD
BuildArch:      noarch

# The PS1 cutscene amd briefing voice over is available for download,
# but is still proprietary data
Source100:      %{addon_url}/movies/extra-movies.7z
Source101:      %{addon_url}/gamedata/extra-hog.7z
Source200:      %{addon_url}/gamedata/D2-demodata.7z

%description
Data for Descent 1 and 2

%package d1
Summary:        Descent 1 Data
Version:        1.4a

%description d1
Descent 1 data files


%package d1-cutscenes
Summary:        Descent 1 Cutscenes
Version:        1.0
Requires:       %{name}-d1

%description d1-cutscenes
Descent 1 briefing and intro cutscenes from the playstation version


%package d2
Summary:        Descent 2 Data
Version:        1.2
Provides:       descent2-data = 1.2

%description d2
Descent 2 data files


%package d2-vertigo
Summary:        Descent 2 Vertigo missions
Version:        1.0
Requires:       %{name}-d2

%description d2-vertigo
Descent 2 Vertigo missions

%package d2-demo
Summary:        Descent 2 Demo
Version:        1.0
# No known license for these, but considering most proprietary demos, it's got to be non-commercial
License:        Shareware
Provides:       descent2-data = 1.0demo

%description d2-demo
Descent 2 demo files

%install
# create dir structure
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{output_path}/data
mkdir $RPM_BUILD_ROOT%{output_path}/missions
mkdir $RPM_BUILD_ROOT%{output_path}/movies

# d1
cp %{input_path}/data/descent.{hog,pig} $RPM_BUILD_ROOT%{output_path}/data/

# d1-cutscenes
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE100} > /dev/null
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE101} > /dev/null

# d2
cp %{input_path}/data/descent2.{ham,hog,s11,s22} $RPM_BUILD_ROOT%{output_path}/data/
cp %{input_path}/data/{alien{1,2},fire,groupa,ice,water}.pig $RPM_BUILD_ROOT%{output_path}/data/
cp %{input_path}/data/hoard.ham $RPM_BUILD_ROOT%{output_path}/data/
cp %{input_path}/movies/{intro,other,robots}-h.mvl $RPM_BUILD_ROOT%{output_path}/movies/

# d2-vertigo
cp %{input_path}/missions/d2x.{hog,mn2} $RPM_BUILD_ROOT%{input_path}/missions/
cp %{input_path}/movies/d2x-h.mvl $RPM_BUILD_ROOT%{input_path}/movies/

# d2-demo
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE200} > /dev/null
cd $RPM_BUILD_ROOT%{output_path}/data && for f in D2DEMO.*; do mv "$f" "`echo $f | tr "[:upper:]" "[:lower:]"`"; done

%clean
rm -rf $RPM_BUILD_ROOT

%files d1
%defattr(644,root,root,755)
%{output_path}/data/descent.hog
%{output_path}/data/descent.pig

%files d1-cutscenes
%defattr(644,root,root,755)
%{output_path}/data/extra.hog
%{output_path}/movies/extra*-h.mvl

%files d2
%defattr(644,root,root,755)
%dir %{output_path}
%dir %{output_path}/data
%{output_path}/data/descent2.ham
%{output_path}/data/descent2.hog
%{output_path}/data/descent2.s11
%{output_path}/data/descent2.s22
%{output_path}/data/alien1.pig
%{output_path}/data/alien2.pig
%{output_path}/data/fire.pig
%{output_path}/data/groupa.pig
%{output_path}/data/ice.pig
%{output_path}/data/water.pig
%{output_path}/data/hoard.ham
%dir %{output_path}/missions
%dir %{output_path}/movies
%{output_path}/movies/intro-h.mvl
%{output_path}/movies/other-h.mvl
%{output_path}/movies/robots-h.mvl

%files d2-vertigo
%defattr(644,root,root,755)
%{output_path}/missions/d2x.hog
%{output_path}/missions/d2x.mn2
%{output_path}/movies/d2x-h.mvl

%files d2-demo
%defattr(644,root,root,755)
%{output_path}/data/d2demo.ham
%{output_path}/data/d2demo.hog
%{output_path}/data/d2demo.pig

%changelog
* Tue Aug 05 2014 Aaron Kling <webgeek1234@gmail.com> - 1.0-1
- Cleanup spec and properly own folders
* Thu May 08 2014 Aaron Kling <webgeek1234@gmail.com> - 1.0-1
- Initial spec

