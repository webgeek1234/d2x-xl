%global commit a1e1593039763915be2845cd2cbf81cd8691454b
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%define output_path /usr/share/descent2
%define data_ver    1.18.64

Name:           d2x-xl
Version:        1.18.72
Release:        1%{dist}
Summary:        6-DOF Shooter
# The initial code released by Parallex had a non-commercial restriction
License:        Non-commercial
URL:            https://github.com/webgeek1234/d2x-xl
Source0:        https://github.com/webgeek1234/%{name}/archive/%{commit}/%{name}-%{commit}.tar.gz
Source1:        http://www.descent2.de/files/%{name}-data-%{data_ver}.7z
BuildRequires:  SDL-devel SDL_image-devel SDL_mixer-devel SDL_net-devel p7zip cmake gcc-c++ glew-devel zlib-devel curl-devel
Requires:       descent2-data fluid-soundfont-lite-patches

%description
A six degrees of freedom shooter by Parallex with many modern enhancements

%prep
%setup -qn %{name}-%{commit}

%build
mkdir build
cd build
%cmake -DDEVELOPMENT_BUILD=Off ..
make %{?_smp_mflags}

%install
# create dir structure
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_bindir}
mkdir -p $RPM_BUILD_ROOT%{output_path}
mkdir -p $RPM_BUILD_ROOT%{output_path}/music/d1
mkdir $RPM_BUILD_ROOT%{output_path}/music/d2
mkdir -p $RPM_BUILD_ROOT%{output_path}/sounds/d1
mkdir -p $RPM_BUILD_ROOT%{output_path}/sounds/d2/22khz
mkdir $RPM_BUILD_ROOT%{output_path}/sounds/d2/44khz
mkdir -p $RPM_BUILD_ROOT%{output_path}/textures/d1
mkdir -p $RPM_BUILD_ROOT/var/cache/d2x-xl

# Copy bin and script
cp build/d2x-xl* $RPM_BUILD_ROOT%{_bindir}/

# Extract data and clean unneeded files
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE1} > /dev/null
rm -rf $RPM_BUILD_ROOT%{output_path}/config
rm -rf $RPM_BUILD_ROOT%{output_path}/profiles

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(644, root, root, 755)
%attr(755, root, root) %{_bindir}/d2x-xl*
%doc COPYING
%doc COPYING.*
%doc README
%doc README.karx11erx
%dir %{output_path}/data/d2x-xl
%{output_path}/data/d2x-xl.hog
%{output_path}/data/exit.ham
%{output_path}/data/descent.tex.eng
%{output_path}/data/descent.tex.ger
%{output_path}/data/d2x-xl/IpToCountry-Default.csv
%dir %{output_path}/models
%{output_path}/models/bullet.ase
%{output_path}/models/bullet.tga
%dir %{output_path}/music
%dir %{output_path}/music/d1
%dir %{output_path}/music/d2
%dir %{output_path}/sounds
%dir %{output_path}/sounds/d1
%dir %{output_path}/sounds/d2
%dir %{output_path}/sounds/d2/22khz
%dir %{output_path}/sounds/d2/44khz
%dir %{output_path}/sounds/d2x-xl
%{output_path}/sounds/d2/afbr_1.wav
%{output_path}/sounds/d2x-xl/*.wav
%dir %{output_path}/textures
%dir %{output_path}/textures/d1
%dir %{output_path}/textures/d2
%dir %{output_path}/textures/d2x-xl
%{output_path}/textures/d2/*.tga
%{output_path}/textures/d2x-xl/*.tga
%dir %attr(755, root, games) /var/cache/d2x-xl

%changelog
* Tue Aug 05 2014 Aaron Kling <webgeek1234@gmail.com> - 1.17.73-1
- More upstream changes and spec file cleanup
* Wed Jun 18 2014 Aaron Kling <webgeek1234@gmail.com> - 1.17.43-1
- A lot of upstream changes and cleanup
* Mon Jun 09 2014 Aaron Kling <webgeek1234@gmail.com> - 1.17.35-1
- Many program updates and folder restructure
* Thu May 08 2014 Aaron Kling <webgeek1234@gmail.com> - 1.17.13-1
- Initial spec

