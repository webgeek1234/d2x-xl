# Note: assuming that this file is included in CMakeLists.txt, so
#       using IS_WIN32, IS_APPLE, IS_LINUX
# Setup the packer

set(CPACK_PACKAGE_VERSION_MAJOR ${D2X_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${D2X_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${D2X_MICRO})
set(CPACK_PACKAGE_NAME "d2x-xl")
set(CPACK_PACKAGE_VENDOR "d2x-xl authors")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "d2x-xl")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Descent 2 XL")
set(CPACK_PACKAGE_DESCRIPTION "A six degrees of freedom shooter by Parallex with many modern enhancements")

if(IS_APPLE)
  set(APP_RESOURCES_PATH ${CMAKE_CURRENT_BINARY_DIR}/$(CONFIGURATION)/${D2X_EXE}.app/Contents/Resources)
  set(APP_FRAMEWORKS_PATH ${CMAKE_CURRENT_BINARY_DIR}/$(CONFIGURATION)/${D2X_EXE}.app/Contents/Frameworks)
  add_custom_command(TARGET ${D2X_EXE} POST_BUILD
    COMMAND rm -rf ${APP_RESOURCES_PATH}
    COMMAND mkdir -p ${APP_RESOURCES_PATH}
    COMMAND cp ${PROJECT_SOURCE_DIR}/packaging/d2.icns ${APP_RESOURCES_PATH})
endif(IS_APPLE)

if(NOT IS_APPLE) # a license agreement popping up whenever people open the DMG will drive them crazy
  set(CPACK_RESOURCE_FILE_LICENSE ${PROJECT_SOURCE_DIR}/COPYING)
endif()

set(CPACK_RESOURCE_FILE_README ${PROJECT_SOURCE_DIR}/README)
set(CPACK_RESOURCE_FILE_WELCOME ${PROJECT_SOURCE_DIR}/README)

if (${DEVELOPMENT_BUILD} MATCHES "On")
  set(CPACK_PACKAGE_FILE_NAME d2x-xl-development-mode-no-distribution)
else()
  if(CMAKE_BUILD_TYPE MATCHES Debug)
    set(CPACK_PACKAGE_FILE_NAME d2x-xl-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-debug-withpdbs)
  elseif(CMAKE_BUILD_TYPE MATCHES RelWithDebInfo)
    set(CPACK_PACKAGE_FILE_NAME d2x-xl-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-withpdbs)
  else()
    set(CPACK_PACKAGE_FILE_NAME d2x-xl-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH})
  endif()
endif()

if( IS_WIN32 AND NOT UNIX )
  set(CPACK_PACKAGE_EXECUTABLES ${D2X_EXE} "Descent 2 XL")
endif()

if(IS_APPLE)
# currently can't get bundle name and long version string to display, grr
#  set(MACOSX_BUNDLE_BUNDLE_NAME "wxLauncher")
#  set(MACOSX_BUNDLE_LONG_VERSION_STRING "wxLauncher for the SCP, version ${MACOSX_BUNDLE_SHORT_VERSION_STRING}")
  set(MACOSX_BUNDLE_ICON_FILE d2.icns)
  set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH})
  set(MACOSX_BUNDLE_COPYRIGHT "Copyright ${CPACK_PACKAGE_VENDOR}")
endif(IS_APPLE)

if(IS_APPLE)
  set(CPACK_BINARY_DRAGNDROP "ON")
  set(CPACK_BINARY_PACKAGEMAKER "OFF")
  set(CPACK_BINARY_STGZ "OFF")
  set(CPACK_BINARY_TZ "OFF")
  set(CPACK_BINARY_TGZ "OFF")
endif()

include(CPack)

if(IS_APPLE)
  install(TARGETS ${D2X_EXE} 
	RUNTIME DESTINATION .
	BUNDLE DESTINATION .)
endif()

if(IS_APPLE)
  install(FILES ${PROJECT_SOURCE_DIR}/COPYING DESTINATION .)
  install(FILES ${PROJECT_SOURCE_DIR}/COPYING.d2x DESTINATION .)
  install(FILES ${PROJECT_SOURCE_DIR}/COPYING.karx11erx DESTINATION .)
  install(FILES ${PROJECT_SOURCE_DIR}/COPYING.parallex DESTINATION .)
endif()
