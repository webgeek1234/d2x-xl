%define output_path /usr/share/descent2
%define addon_url   http://descent2.de/files
%define sc55_url    http://sc55.duke4.net

Name:           d2x-xl-upgraded
Version:        1.1
Release:        2%{dist}
Summary:        Upgrade data for d2x-xl
# The initial code released by Parallex had a non-commercial restriction
License:        Non-commercial
BuildArch:      noarch
BuildRequires:  p7zip unzip
URL:            http://descent2.de/downloads.html

# Models
Source100:      %{addon_url}/models/hires-models.7z

# Textures
Source220:      %{addon_url}/textures/D1-textures-512x512.7z
Source240:      %{addon_url}/textures/D2-textures-512x512.7z

# Sound and Music
Source300:      %{addon_url}/sound/hires-sounds.7z
Source350:      %{sc55_url}/ogg/descent1_sc55.zip
Source351:      %{sc55_url}/ogg/descent2_sc55.zip
Source352:      %{sc55_url}/flac/descent1_flac.zip
Source353:      %{sc55_url}/flac/descent2_flac.zip

%description
Enhancement data for d2x-xl

%package models
Summary:        High Poly Models
Requires:       d2x-xl

%description models
High poly models of weapons, ships, and such by the d2x-xl community

%package sounds-d1
Summary:        High Quality Sounds for Descent 1
Requires:       d2x-xl

%description sounds-d1
Re-sampled 44KHz sounds for Descent 1

%package sounds-d2
Summary:        High Quality Sounds for Descent 2
Requires:       d2x-xl

%description sounds-d2
Re-sampled 44KHz sounds for Descent 2

%package music-d1-ogg
Summary:        Rendered Music in OGG for Descent 1
Requires:       d2x-xl
Conflicts:      d2x-xl-music-d1-flac

%description music-d1-ogg
Descent 1 Music rendered to OGG from a Roland synthesizer

%package music-d1-flac
Summary:        Rendered Music in FLAC for Descent 1
Requires:       d2x-xl
Conflicts:      d2x-xl-music-d1-ogg

%description music-d1-flac
Descent 1 Music rendered to FLAC from a Roland synthesizer

%package music-d2-ogg
Summary:        Rendered Music in OGG for Descent 2
Requires:       d2x-xl
Conflicts:      d2x-xl-music-d2-flac

%description music-d2-ogg
Descent 2 Music rendered to OGG from a Roland synthesizer

%package music-d2-flac
Summary:        Rendered Music in FLAC for Descent 2
Requires:       d2x-xl
Conflicts:      d2x-xl-music-d2-ogg

%description music-d2-flac
Descent 2 Music rendered to OGG from a Roland synthesizer

%package textures-d1
Summary:        High Resolution Textures for Descent 1
Requires:       d2x-xl

%description textures-d1
Descent 1 textures redone by the d2x-xl community

%package textures-d2
Summary:        High Resolution Textures for Descent 2
Requires:       d2x-xl

%description textures-d2
Descent 2 textures redone by the d2x-xl community

%install
# create dir structure
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{output_path}/music/d1
mkdir $RPM_BUILD_ROOT%{output_path}/music/d2

# Models
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE100} > /dev/null
rm -f $RPM_BUILD_ROOT%{output_path}/models/*.txt
rm -f $RPM_BUILD_ROOT%{output_path}/models/bullet.*

# Textures
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE220} > /dev/null
rm -f $RPM_BUILD_ROOT%{output_path}/textures/d1/*.bmp
rm -f $RPM_BUILD_ROOT%{output_path}/textures/d1/drsp-readme.txt
rm -f $RPM_BUILD_ROOT%{output_path}/textures/d1/Thumbs.db
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE240} > /dev/null
rm -f $RPM_BUILD_ROOT%{output_path}/textures/d2/*.jpg
rm -f $RPM_BUILD_ROOT%{output_path}/textures/d2/{bullettime#0,cockpit{,b},monsterball,slowmotion#0,status{,b}}.tga

# Sounds and music
7za x -o$RPM_BUILD_ROOT%{output_path} %{SOURCE300} > /dev/null
rm -rf $RPM_BUILD_ROOT%{output_path}/sounds/d2x-xl
rmdir $RPM_BUILD_ROOT%{output_path}/sounds/d1/d2
rmdir $RPM_BUILD_ROOT%{output_path}/sounds/d2/44khz/44khz
rmdir $RPM_BUILD_ROOT%{output_path}/sounds/d2/44khz/d2x-xl
unzip %{SOURCE350} -d $RPM_BUILD_ROOT%{output_path}/music/d1 > /dev/null
unzip %{SOURCE351} -d $RPM_BUILD_ROOT%{output_path}/music/d2 > /dev/null
unzip %{SOURCE352} -d $RPM_BUILD_ROOT%{output_path}/music/d1 > /dev/null
unzip %{SOURCE353} -d $RPM_BUILD_ROOT%{output_path}/music/d2 > /dev/null
mv $RPM_BUILD_ROOT%{output_path}/music/d1/descent.flac $RPM_BUILD_ROOT%{output_path}/music/d1/title.flac
mv $RPM_BUILD_ROOT%{output_path}/music/d1/descent.ogg $RPM_BUILD_ROOT%{output_path}/music/d1/title.ogg
mv $RPM_BUILD_ROOT%{output_path}/music/d2/descent.flac $RPM_BUILD_ROOT%{output_path}/music/d2/title.flac
mv $RPM_BUILD_ROOT%{output_path}/music/d2/descent.ogg $RPM_BUILD_ROOT%{output_path}/music/d2/title.ogg

# Generate playlists for music
echo "game01.ogg" > $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game02.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game03.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game04.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game05.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game06.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game07.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game08.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game09.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game10.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game11.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game12.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game13.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game14.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game15.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game16.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game17.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game18.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game19.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game20.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game21.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game22.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt
echo "game01.ogg" > $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_ogg.txt
echo "game02.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_ogg.txt
echo "game03.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_ogg.txt
echo "game04.ogg" >> $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_ogg.txt
cp $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_ogg.txt $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_flac.txt
sed -i 's|ogg|flac|' $RPM_BUILD_ROOT%{output_path}/music/d2/d2_playlist_flac.txt
cp $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_ogg.txt $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_flac.txt
sed -i 's|ogg|flac|' $RPM_BUILD_ROOT%{output_path}/music/d1/d1_playlist_flac.txt

# Generate readmes
echo "Courtesy of MetalBeast and ~Joffa~" > $RPM_BUILD_ROOT%{output_path}/models/README
echo "Rendered by Brandon Blume" > $RPM_BUILD_ROOT%{output_path}/music/d1/README_OGG
echo "Rendered by Brandon Blume" > $RPM_BUILD_ROOT%{output_path}/music/d1/README_FLAC
echo "Rendered by Brandon Blume" > $RPM_BUILD_ROOT%{output_path}/music/d2/README_OGG
echo "Rendered by Brandon Blume" > $RPM_BUILD_ROOT%{output_path}/music/d2/README_FLAC
echo "Unknown resampler" > $RPM_BUILD_ROOT%{output_path}/sounds/d2/README
echo "Unknown resampler" > $RPM_BUILD_ROOT%{output_path}/sounds/d1/README
echo "Courtesy of Aus-RED-5, DizzyRox, MetalBeast, Novacron, TheftBot" > $RPM_BUILD_ROOT%{output_path}/textures/d2/README
echo "Courtesy of Aus-RED-5, DizzyRox, Novacron" > $RPM_BUILD_ROOT%{output_path}/textures/d1/README

%clean
rm -rf $RPM_BUILD_ROOT

%post music-d1-ogg
mv %{output_path}/music/d1/d1_playlist_ogg.txt %{output_path}/music/d1/playlist.txt

%post music-d1-flac
mv %{output_path}/music/d1/d1_playlist_flac.txt %{output_path}/music/d1/playlist.txt

%post music-d2-ogg
mv %{output_path}/music/d2/d2_playlist_ogg.txt %{output_path}/music/d2/playlist.txt

%post music-d2-flac
mv %{output_path}/music/d2/d2_playlist_flac.txt %{output_path}/music/d2/playlist.txt

%preun music-d1-ogg
mv %{output_path}/music/d1/playlist.txt %{output_path}/music/d1/d1_playlist_ogg.txt

%preun music-d1-flac
mv %{output_path}/music/d1/playlist.txt %{output_path}/music/d1/d1_playlist_flac.txt

%preun music-d2-ogg
mv %{output_path}/music/d2/playlist.txt %{output_path}/music/d2/d2_playlist_ogg.txt

%preun music-d2-flac
mv %{output_path}/music/d2/playlist.txt %{output_path}/music/d2/d2_playlist_flac.txt

%files models
%defattr(644, root, root, 755)
%{output_path}/models/*.ase
%{output_path}/models/*.tga
%doc %{output_path}/models/README

%files sounds-d1
%defattr(644, root, root, 755)
%{output_path}/sounds/d1/*.wav
%doc %{output_path}/sounds/d1/README

%files sounds-d2
%defattr(644, root, root, 755)
%{output_path}/sounds/d2/22khz/*.wav
%{output_path}/sounds/d2/44khz/*.wav
%doc %{output_path}/sounds/d2/README

%files music-d1-ogg
%defattr(644, root, root, 755)
%{output_path}/music/d1/d1_playlist_ogg.txt
%{output_path}/music/d1/*.ogg
%doc %{output_path}/music/d1/README_OGG

%files music-d1-flac
%defattr(644, root, root, 755)
%{output_path}/music/d1/d1_playlist_flac.txt
%{output_path}/music/d1/*.flac
%doc %{output_path}/music/d1/README_FLAC

%files music-d2-ogg
%defattr(644, root, root, 755)
%{output_path}/music/d2/d2_playlist_ogg.txt
%{output_path}/music/d2/*.ogg
%doc %{output_path}/music/d2/README_OGG

%files music-d2-flac
%defattr(644, root, root, 755)
%{output_path}/music/d2/d2_playlist_flac.txt
%{output_path}/music/d2/*.flac
%doc %{output_path}/music/d2/README_FLAC

%files textures-d1
%defattr(644, root, root, 755)
%{output_path}/textures/d1/*.tga
%doc %{output_path}/textures/d1/README

%files textures-d2
%defattr(644, root, root, 755)
%{output_path}/textures/d2/*.tga
%doc %{output_path}/textures/d2/README

%changelog
* Tue Aug 05 2014 Aaron Kling <webgeek1234@gmail.com> - 1.1-2
- Cleanup spec file and properly own folders
* Thu May 08 2014 Aaron Kling <webgeek1234@gmail.com> - 1.1-1
- Folder restructuring
* Thu May 08 2014 Aaron Kling <webgeek1234@gmail.com> - 1.0-1
- Initial spec

